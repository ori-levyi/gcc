FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN apt update
RUN apt --yes install git
RUN git --version
RUN base64 --version

CMD base64 --version
